import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const SampleText = () => {
    return (
        <View>
            <Text style={styles.textStyle}>Sample Text From Component</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    textStyle: {
        fontWeight: 'bold'
    }
});

export default SampleText;
import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';

import HomeScreen from './HomeScreen';
import DetailsScreen from './DetailScreen';

const AppNavigator = createStackNavigator(
  {
    Home: HomeScreen,
    Details: DetailsScreen,
  },
  {
    initialRouteName: 'Home',
  }
);

export default AppNavigator;
import SampleText from './components/SampleText';

import React from 'react';
import { Button, View, Text } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import HomeScreen from './components/HomeScreen';
import DetailsScreen from './components/DetailScreen';
import AppNavigator from './components/AppNavigator';

export default createAppContainer(AppNavigator);
/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import SampleText from './components/SampleText';
import HomeScreen from './components/HomeScreen';
import DetailsScreen from './components/DetailScreen';
import AppNavigator from './components/AppNavigator';

AppRegistry.registerComponent(appName, () => App);

export {
    HomeScreen, 
    DetailsScreen, 
    AppNavigator,
    SampleText
};
